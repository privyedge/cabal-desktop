# Cabal Desktop
# THIS IS NOT THE OFFICIAL REPOSITORY, THIS REPO IS UNSTABLE, UNTESTED AND SHOULD BE ONLY USED FOR TESTING PURPOSE.
# We are currently testing lot's fo things Please your the official git repo if you are interested in cabal https://github.com/cabal-club/cabal-desktop 
# Privy is looking for an ideal decentralized way to get rid of matrix federations, we love matrix protocol unfortunately We do not like the idea to been tied to their "federation" which look more like a distributed network than a decentralized.
#TODO
integrate matrix-react-sdk
Integrating matrix-js-sdk

#DONE

#GOALS ---- random brain storming 
- Integrating matrix protocols, which is compatible with a varity of other protocols, easy to bridge with other chat.
- eventually maybe we can consider adding some scuttlebutt features from patchwork or patchbay
- Integrating blockstack auth to give more control to the users over their data.
- It would be fun to be able to use gaia, ipfs aswell 
- holochain integration , project like holochain-basic-chat could be integrated,i feel like if an apps doesn't only provide one 
decentralization storage, networking, auth methods, without requiring you to download another clients would be great.
> Desktop client for cabal, the p2p/decentralized/offline-first chat platform.

<center><img src="screenshot.png"/></center>

## Install

### Download the latest release

https://github.com/cabal-club/cabal-desktop/releases/

### Build from source

```
$ git clone https://github.com/cabal-club/cabal-desktop
$ cd cabal-desktop

$ npm install             # install dependencies
$ npm start               # start the application
```

## Distribute

build for current platform:

```
$ npm run dist
```

build for [multiple platforms](https://www.electron.build/multi-platform-build#docker):

```
$ ./bin/build-multi
```
https://github.com/cabal-club/cabal-desktop
